package controllers.ex1

import play.api.data.Form
import play.api.data.Forms._
import play.api.db.DB
import play.api.Play.current

object Example {

  case class UserData(email: String, password: String)

  val userForm = Form(
    mapping(
      "email" -> text,
      "password" -> text
    )(UserData.apply)(UserData.unapply)
  )

  object Wrong {

    val request = userForm.bindFromRequest().get
    val email = request.email
    val password = request.password
    val sql = "select * from users where (email ='" + email + "' and password ='" + password + "')"
    DB.withConnection {
      conn => {
        val statement = conn.createStatement()
        val result = statement.executeQuery(sql)
        if (result.next()) {

          // Successfully logged in and redirect to user profile page

        } else {

          // Auth failure -Redirect to Login Page
        }

      }
    }
  }

  object Right {
    val request = userForm.bindFromRequest().get
    val email = request.email
    val password = request.password
    val sql = "select * from users where email = ? and password = ? "

    DB.withConnection {
      conn => {
        val preparedStatement = conn.prepareStatement(sql)

        preparedStatement.setString(1, email)
        preparedStatement.setString(2, password)

        val result = preparedStatement.executeQuery()
        if (result.next()) {

          // Successfully logged in and redirect to user profile page

        } else {

          // Auth failure -Redirect to Login Page
        }

      }
    }

  }


}
