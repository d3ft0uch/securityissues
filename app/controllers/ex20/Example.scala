package controllers.ex20

import play.api.mvc._


object Example {


  object Wrong extends Controller {
    def redirect = Action {
      implicit request =>
        val url = request.getQueryString("url")
        url match {
          case Some(addr) => Redirect(addr, 302)
          case _ => BadRequest()
        }
    }
  }


  object Right extends Controller {

    def redirect = Action {
      implicit request =>
        val url = request.getQueryString("url")
        url match {
          case Some(addr) if addr == "https://sso.codebashing.com" => Redirect(addr, 302)
          case _ => BadRequest()
        }
    }
  }

}

