package controllers.ex4

import play.api.mvc._

object Example {


  object Wrong extends Controller {

    def securedAction = Action {
      implicit request =>
        val sessionID = request.getQueryString("app_session_id").get
        if (isSessionValid(sessionID)) {
          Ok(views.html.index())
        }
        else {
          Unauthorized()
        }
    }

    def isSessionValid(session: String) = {
      // some checking here ...
      true
    }

  }


  object Right extends Controller {

    def securedAction = Action {
      implicit request =>
        val sessionID = request.session.get("app_session_id").get
        if (isSessionValid(sessionID)) {
          Ok(views.html.index())
        }
        else {
          Unauthorized()
        }
    }

    def isSessionValid(session: String) = {
      // some checking here ...
      true
    }

  }

}

