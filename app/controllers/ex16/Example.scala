package controllers.ex16

import play.api.mvc._


object Example {


  object Wrong extends Controller {

    case class User(name: String, role: Role.Value)

    object Role extends Enumeration {
      val User, Admin = Value
    }

    def showUsers = Action {
      val currentUser = getUser()
      if (authenticated(currentUser)) {
        val users = UserDAO.readAll()
        Ok(views.html.dashboard(users))
      }
      else {
        Forbidden()
      }
    }

    def getUser() = User("bob", Role.User)

    def authenticated(user: User) = true
  }


  object Right extends Controller {

    case class User(name: String, role: Role.Value)

    object Role extends Enumeration {
      val User, Admin = Value
    }

    def showUsers = Action {
      val currentUser = getUser()
      if (authenticated(currentUser) && currentUser.role == Role.Admin) {
        val users = UserDAO.readAll()
        Ok(views.html.dashboard(users))
      }
      else {
        Forbidden()
      }
    }

    def getUser() = User("bob", Role.User)

    def authenticated(user: User) = true
  }

}

