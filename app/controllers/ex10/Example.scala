package controllers.ex10

import play.api.mvc._


object Example {


  object Wrong extends Controller {

    // no code at all, similar as Java

  }


  object Right extends Controller {

    // no code at all, similar as Java

  }

}

