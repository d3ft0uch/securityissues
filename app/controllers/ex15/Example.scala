package controllers.ex15

import play.api.db.DB
import play.api.mvc._


object Example {


  object Wrong extends Controller {

    case class User(name: String, email: String, phone: String, department: String)

    def getProfile = Action {
      implicit request =>
        val uid = request.getQueryString("uid").get
        val qString = "select * from users where userid = ?"
        DB.withConnection {
          conn => {
            val preparedStatement = conn.prepareStatement(qString)
            preparedStatement.setString(1, uid)
            val result = preparedStatement.executeQuery()
            if (result.first()) {
              val user = User(
                result.getString("name"),
                result.getString("email"),
                result.getString("phone"),
                result.getString("department")
              )
              Ok(user)
            }
            else {
              BadRequest()
            }
          }
        }
    }

  }


  object Right extends Controller {

    case class User(name: String, email: String, phone: String, department: String)

    def getProfile = Action {
      implicit request =>
        val uid = request.getQueryString("uid").get
        val username = request.session.get("username").get
        val qString = "select * from users where userid = ? and name = ?"
        DB.withConnection {
          conn => {
            val preparedStatement = conn.prepareStatement(qString)
            preparedStatement.setString(1, uid)
            preparedStatement.setString(2, username)
            val result = preparedStatement.executeQuery()
            if (result.first()) {
              val user = User(
                result.getString("name"),
                result.getString("email"),
                result.getString("phone"),
                result.getString("department")
              )
              Ok(user)
            }
            else {
              BadRequest()
            }
          }
        }
    }

  }

}

