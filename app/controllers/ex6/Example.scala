package controllers.ex6

import play.api.mvc._

object Example {


  object Wrong extends Controller {
    def index() = Action {
      implicit request =>
        val search = request.getQueryString("search").get
        Ok(search)
    }


  }


  object Right extends Controller {
    //    application.conf in Play application ->
    //    add future.escapeInTemplates=true
    //    but since 1.0.1 Play automatically escapes strings

  }

}

