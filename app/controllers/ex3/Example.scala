package controllers.ex3

import java.util.regex.Pattern


object Example {


  object Wrong {

    object CommandExecuter {

      def executeCommand(userName: String) = {
        try {
          val myUid = userName
          val rt = Runtime.getRuntime
          rt.exec(s"/usr/bin/statlab -$myUid") // Call statlab with Alice's username

          // process results for userID and and return output in HTML.
          // ...
        }
        catch {
          case e: Exception =>
        }
      }
    }

  }

  object Right {

    object CommandExecuter {

      def executeCommand(userName: String) = {
        try {
          val myUid = userName
          if (!Pattern.matches("[0-9A-Za-z]+", myUid)) ""
          else {
            val rt = Runtime.getRuntime
            rt.exec(s"/usr/bin/statlab -$myUid") // Call statlab with Alice's username

            // process results for userID and and return output in HTML.
            // ...
          }

        }
        catch {
          case e: Exception =>
        }
      }
    }

  }

}
