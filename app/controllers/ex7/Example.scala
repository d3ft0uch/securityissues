package controllers.ex7

import play.api.mvc._

object Example {


  object Wrong extends Controller {

    case class Contact(name: String, title: String, number: String)

    // By default, since Play 1.0.1 automatically escapes string to prevent XSS,
    // but be careful with usage Html type in rendering
  }


  object Right extends Controller {


  }

}

