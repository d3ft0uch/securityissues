package controllers.ex9

import java.io.File

import play.api.mvc._

import scala.io.Source


object Example {


  object Wrong extends Controller {

    def file() = Action {
      implicit request =>
        val filename = request.getQueryString("file")
        val file = new File(s"/tmp/$filename")
        val content = Source.fromFile(file).mkString
        Ok(content)
    }

  }


  object Right extends Controller {
    def file() = Action {
      implicit request =>
        val filename = request.getQueryString("file")
        val file = new File(s"/tmp/$filename")
        val canonicalPath = file.getCanonicalPath
        if (!canonicalPath.startsWith("/tmp/"))
          Forbidden()
        else {
          val content = Source.fromFile(file).mkString
          Ok(content)
        }


    }


  }

}

