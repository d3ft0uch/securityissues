package controllers.ex5

import java.security.SecureRandom

import org.apache.commons.codec.binary.Hex
import play.api.mvc._

object Example {


  object Wrong extends Controller {

    def newSession = {
      System.currentTimeMillis().toString
    }

  }


  object Right extends Controller {
    def newSession = {
      //      System.currentTimeMillis().toString
      val rand = new SecureRandom()
      val bytes = Array[Byte](20)
      rand.nextBytes(bytes)
      new String(Hex.encodeHex(bytes))
    }


  }

}

