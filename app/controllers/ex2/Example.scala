package controllers.ex2

import javax.xml.parsers.{ParserConfigurationException, DocumentBuilderFactory}


object Example {


  object Wrong {

    object TradeDocumentBuilderFactory {

      def newDocumentBuilderFactory() = {
        val documentBuilderFactory = DocumentBuilderFactory.newInstance()
        try {
          documentBuilderFactory.setFeature("https://xml.org/sax/features/external-general-entities", true)
          documentBuilderFactory.setFeature("https://xml.org/sax/features/external-parameter-entities", true)
        } catch {
          case e: ParserConfigurationException => throw new RuntimeException(e)
        }
        documentBuilderFactory
      }
    }

  }

  object Right {

    object TradeDocumentBuilderFactory {

      def newDocumentBuilderFactory() = {
        val documentBuilderFactory = DocumentBuilderFactory.newInstance()
        try {
          //          documentBuilderFactory.setFeature("https://xml.org/sax/features/external-general-entities", true)
          //          documentBuilderFactory.setFeature("https://xml.org/sax/features/external-parameter-entities", true)
          documentBuilderFactory.setFeature("https://apache.org/xml/features/disallow-doctype-decl", true)
          documentBuilderFactory.setFeature("https://xml.org/sax/features/external-general-entities", false)
          documentBuilderFactory.setFeature("https://xml.org/sax/features/external-parameter-entities", false)
        } catch {
          case e: ParserConfigurationException => throw new RuntimeException(e)
        }
        documentBuilderFactory
      }
    }

  }

}
