package controllers.ex11

import play.api.Mode
import play.api.mvc._


object Example {


  object Wrong extends Controller {

    def login = Action {
      implicit request =>
        val debug = request.getQueryString("debug") match {
          case Some(1) => Ok(views.html.index("Debug mode - no auth"))
          case _ =>
            // not in debug
            Ok()
        }
        /// ....

        Ok()

    }

  }


  object Right extends Controller {
    def login = Action {
      implicit request =>
        val debug = request.getQueryString("debug") match {
            // added mode checking
          case Some(1) if play.api.Play.current.mode != Mode.Prod => Ok(views.html.index("Debug mode - no auth"))
          case _ =>
            // not in debug
            Ok()
        }
        /// ....

        Ok()

    }

  }

}

